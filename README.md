# REDIA: World Heritage Case

The site pretty much includes the modern version of everything needed to operate all animations on devices etc. They are easily updated with bower.

SCSS/Compass are used to compile the stylesheets, so any editing is probably wise to do in this, unless on future updates it will get wiped out. PNGs are optimized with [TinyPNG] [png]: 

Site was built on Twitter bootstrap, update if needed using bower.

It's pretty modular, all elements are in ```partials/``` and is just included in ```index.php```. There are no view routes (MVC patterns etc.) like planned, the site is too small for this type of framework. 


[png]:http://www.tinypng.com/
[Twitter Bootstrap]:http://twitter.github.com/bootstrap/
[jQuery]:http://jquery.com


-Ryan