require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = " "
css_dir = "css"
sass_dir = "scss"
images_dir = "img"
javascripts_dir = "js"

relative_assets = true

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

output_style = :expanded
line_comments = true

# This is where the magic happens, nothing too fancy though...
on_stylesheet_saved do
  `compass compile -c config_prod.rb --force`
end