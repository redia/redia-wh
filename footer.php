<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 24/02/15
 * Time: 17:05
 */

?>
		<!-- Load all relevant scripts  -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

		<script src="js/vendor/bootstrap.min.js"></script>
		<script src="js/parallax/deploy/jquery.parallax.min.js"></script>

		<script src="js/fancybox/source/jquery.fancybox.pack.js"></script>
		<script src="js/fancybox/source/helpers/jquery.fancybox-media.js"></script>

		<script type="text/javascript" src="js/vendor/jquery-hotspotter.min.js"></script>
		<script type="text/javascript" src="js/vendor/lazyload-min.js"></script>

		<script type="text/javascript" src="js/wow/dist/wow.js"></script>


		<!-- Init Custom loads... -->
		<script src="js/main.js"></script>

		<script>
			(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
				function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
				e=o.createElement(i);r=o.getElementsByTagName(i)[0];
				e.src='//www.google-analytics.com/analytics.js';
				r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
			ga('create','UA-60802272-1','auto');ga('send','pageview');
		</script>

	</body>
</html>
