<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 24/02/15
 * Time: 17:04
 */
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>World Heritage App</title>

        <link rel="shortcut icon" href="img/wh_fav_icon_32px.png"/>

        <meta name="description" content="">
        <meta name="viewport" content="width=1200, maximum-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="js/fancybox/source/jquery.fancybox.css">
        <link rel="stylesheet" type="text/css" href="css/jquery-hotspotter.min.css" />
        <link rel="stylesheet" type="text/css" href="js/wow/css/libs/animate.min.css" />

      <link rel="stylesheet" type="text/css" href="css/main.css" />
<!--        <link rel="stylesheet" type="text/css" href="css/main.min.css" />-->
        <!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="css/ie7-8.min.css" /><![endif]-->



        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->