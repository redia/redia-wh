
// SECTION 7 VIDEO SUPPORT

var vid = document.getElementById('video-360');
vid.addEventListener('mouseover',hoverVideo,false);

function hoverVideo(e) {

    if (vid.getAttribute('controls') != 'true') {
        vid.setAttribute('controls', 'true');
    }

    vid.play();
    vid.removeAttribute('controls');
    vid.addEventListener('mouseout',pauseVid,false);
}

function pauseVid(e) {
    vid.pause();
    //vid.removeEventListener('mouseout', hideVideo);
}



// Flow Control


var dgPull = dgPull || {};

(function($) {$(document).ready(function() {

    $(window).scroll(dgPull.scrollFn);
});
})(jQuery);

(function ( $ ) {
    $.fn.flowUp = function(e,options) {
        var settings = $.extend({
            // Default
            translateY: 150,
            duration: .8,
            externalCSS: false
        }, options);

        $(e).addClass('pullup-element');
        $(e).each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                el.addClass("already-visible");
            }
            else {
                el.addClass('opaque');
            }
        });

        // If external CSS is not used, add CSS to head
        if(!settings.externalCSS)
        {
            $("head").append('<style>.come-in{opacity: 1; -ie-transform:translateY('+settings.translateY+'px);-webkit-transform:translateY('+settings.translateY+'px);transform:translateY('+settings.translateY+'px);-webkit-animation:come-in '+settings.duration+'s ease forwards;animation:come-in '+settings.duration+'s ease forwards}.come-in:nth-child(odd){-webkit-animation-duration:'+settings.duration+'s;animation-duration:'+settings.duration+'s}.already-visible{opacity: 1;-ie-transform:translateY(0);-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation:none;animation:none}@-webkit-keyframes come-in{to{-ie-transform:translateY(0);-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes come-in{to{-ie-transform:translateY(0);-webkit-transform:translateY(0);transform:translateY(0)}} .opaque { opacity: 0; }</style>');
        }
        return this;
    };

    // TO DO: Take out of jQuery Namespace
    $.fn.visible = function(partial) {
        var $t        = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };

}( jQuery ));

dgPull.scrollFn = function() {
    jQuery(".pullup-element").each(function(i, el) {
        var el = jQuery(el);
        if (el.visible(true)) {
            el.addClass("come-in");
            el.removeClass("opaque");
        }});
}



$( document ).ready(function() {
    $('#scene-section3').parallax();
    $('#scene-section6').parallax();

    $("a#single_image").fancybox();



    $('.hs-area').hotspotter();
    $("img.lazy").lazyload({
        placeholder: "",
    });

    $("body").flowUp("section", {
        translateY: 200,        // The amount of pixels that each element will be below the active window.
        duration: 1            // in seconds
    });

    $('.fancybox').fancybox({
        helpers: {
            media: {}
        }
    });

    new WOW().init();

});



$('.vimeoplayer').click(function(){
    $(this).parent().html('<iframe class="vimeoplayerframe" src="https://player.vimeo.com/video/'+$(this).data('vimeoid')+'?portrait=0&title=0&color=bf1f48&badge=0&byline=0&autoplay=1" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
});

