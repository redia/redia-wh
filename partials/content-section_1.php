<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 27/02/15
 * Time: 18:56
 */


?>

<section id="section-1">
	<div class="container">
		<div class="row section-1-header">
			<div class="col-xs-12 text-center">
				<div class="mainlogocontainer">
					<img class="section-1-mainlogo wow fadeIn"  data-wow-duration="3s" src="img/VA-LOGO.png" alt="World Heritage App">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-5 text-center">
				<img class="section-1-sponsor" src="img/LOGOER-Kun-2stk.png" alt="Sponsors">
			</div>
			<div class="col-xs-2 text-center">
				<a href="https://itunes.apple.com/dk/app/danmarks-verdensarv/id878924100" target="_blank">
					<div class="section-1-appicon icon-AppIkon glow  wow fadeIn "></div>
				</a>
			</div>
			<div class="col-xs-5 text-center">
				<a href="https://itunes.apple.com/dk/app/danmarks-verdensarv/id878924100" target="_blank">
					<div class="icon-Appstore glow"></div>
				</a>
			</div>

		</div>

	</div>
</section>