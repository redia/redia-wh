<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 03/03/15
 * Time: 16:35
 */
?>



<section id="section-2">
	<!-- Use a container for the background spanning over two sub-containers -->
	<div class="container">
		<div class="container">
			<div class="row row-header">
				<div class="col-xs-12 text-center">
					<div class='embed-container glow wow fadeIn'>
						<div class="vimeoplayer section-2-play icon-KNAP-Play" data-vimeoid="118588249"></div>
						<img class="vimeoplayer " data-vimeoid="118588249" src="img/PIC-magical.jpg"
						     alt="World Heritage App - Case" width="700">
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row row-header">
				<div class="col-xs-12 text-center">
					<!-- Section Header -->

					<img class="divider" alt="" src="img/icon/redbar.png"/>

					<h1 class="wow fadeIn"  data-wow-duration="3s">A Magical Adventure<br/>Back In Time</h1>

				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 text-center">

					<a id="single_image" rel="lightbox[levels]" href="img/PIC-site-Ros.png">
						<img class="section-2-site glow  clear-animation wow fadeInLeft nudge-up " src="img/PIC-site-Ros-thumb.png" alt="site"/>
					</a>

					<p>ROSKILDE CATHEDRAL</p>

				</div>
				<div class="col-xs-4 text-center">

					<a id="single_image" rel="lightbox[levels]" href="img/PIC-site-Jell.png">
						<img class="section-2-site glow nudge-up wow clear-animation fadeInUp " src="img/PIC-site-Jell-thumb.png" alt="site"/>
					</a>

					<p>ROYAL JELLING</p>

				</div>
				<div class="col-xs-4 text-center">

					<a id="single_image" rel="lightbox[levels]" href="img/PIC-site-Kron.png">
						<img class="section-2-site glow nudge-up wow clear-animation fadeInRight " src="img/PIC-site-Kron-thumb.png" alt="site"/>
					</a>
					<br/>

					<p>KRONBORG</p>


				</div>
			</div>
		</div>
	</div>
</section>