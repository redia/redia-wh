<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 25/02/15
 * Time: 14:01
 */

?>

<section id="section-3">
	<!-- Use a container for the background spanning over two sub-containers -->
	<div class="container">

		<div class="row">
			<div class="col-xs-12 text-center content">
				<!-- Parallax Start -->
				<ul id="scene-section3" class="section-3-scene scene" data-scalar-x="50" data-invert-y="true">
					<li class="layer " data-depth="0.1">
						<div class="section-3-background"></div>
					</li>
				</ul>
			</div>
		</div>

		<div class="container">
			<div class="row row-header">
				<div class="col-xs-12 text-center">

					<!-- Section Header -->

					<img class="divider" alt="" src="img/icon/redbar.png"/>

					<h1 class="wow fadeIn"  data-wow-duration="3s">Discover Historical <br/> Highlights</h1>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-offset-1 col-xs-4">
					<!-- Left Images -->
					<div>
						<a id="single_image" rel="levels" href="img/PIC-levels.png">
							<!--<span class="rollover" ></span>-->
							<img class="section-3 glow clear-animation wow fadeInLeft" src="img/PIC-levels-thumb.png" alt="levels" />
						</a>
					</div>
					<a id="single_image" rel="levels" href="img/PIC-levels-second.png">
						<img class="section-3 glow clear-animation wow fadeInLeft" data-wow-delay="0.2s" src="img/PIC-levels-second-thumb.png" alt="levels"/>
					</a>
				</div>
				<div class="col-xs-7 text-right">
					<!-- Hotspot Image -->

					<div class="hs-area section-3-hotspot">
						<img data-imgdim="1235,799" src="img/IPAD-levels.png" class="section-3-ipad-levels wow fadeInRight">

						<!-- + SPOT -->
						<div class="hs-wrap">
							<div data-activeon="hover" data-coord="270,100" class="img-spot">
								<img src="img/KUGLE-plus.png" height="150" width="150" alt="" class="wow fadeIn">
								<img src="img/KUGLE-plus-on.png" height="150" width="150" alt="">
							</div>
							<div class="tt-wrap">
								<div data-pos="10" data-width="150" data-dir="left" data-anim="goin"
								     class="tip-tooltip">
									<p>Zoom into specific interesting areas.</p>
								</div>
							</div>
						</div>

						<!-- + SPOT -->
						<div class="hs-wrap">
							<div data-activeon="hover" data-coord="650,30" class="img-spot">
								<img src="img/KUGLE-plus.png" height="150" width="150" alt="" class="wow fadeIn">
								<img src="img/KUGLE-plus-on.png" height="150" width="150" alt="">
							</div>
							<div class="tt-wrap">
								<div data-pos="10" data-width="150" data-dir="right" data-anim="goin"
								     class="tip-tooltip"><p>Zoom into specific interesting areas.</p></div>
							</div>
						</div>


						<!-- AUGMENTED SPOT -->
						<div class="hs-wrap">
							<div data-activeon="hover" data-coord="450,200" class="img-spot">
								<img src="img/KUGLE-augmented.png" height="170" width="170" alt="" class="wow fadeIn">
								<img src="img/KUGLE-augmented-on.png" height="170" width="170" alt="">
							</div>
							<div class="tt-wrap">
								<div data-pos="50" data-width="200" data-dir="right" data-anim="goin"
								     class="tip-tooltip"><p>Augmented reality videos</p></div>
							</div>
						</div>

						<!-- 360 SPOT -->
						<div class="hs-wrap">
							<div data-activeon="hover" data-coord="620,350" class="img-spot">
								<img src="img/KUGLE-360.png" height="200" width="200" alt="" class="wow fadeIn" >
								<img src="img/KUGLE-360-on.png" height="200" width="200" alt="">
							</div>
							<div class="tt-wrap">
								<div data-pos="50" data-width="190" data-dir="left" data-anim="goin"
								     class="tip-tooltip"><p>360&deg; Panoramic Views</p></div>
							</div>
						</div>

					</div>

					<!-- End Hotspotter -->

				</div>
			</div>
		</div>


		<!-- IBEACON SECTION  -->


		<div class="container">
			<div class="row row-header">
				<div class="col-xs-12 text-center">
					<!-- Section Header -->

					<img class="divider" alt="" src="img/icon/redbar.png"/>

					<h1 class="wow fadeIn"  data-wow-duration="3s">Storytelling In<br/>The Right Context</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-offset-1 col-xs-5">

					<a id="single_image" rel="lightbox[ibeacon]" href="img/IPAD-Mockup-iBeacon.png">
						<img class="section-3 section-3-ipad glow clear-animation wow fadeInLeft nudge-up" src="img/IPAD-Mockup-iBeacon.png" alt="iBeacon"/>
					</a>

				</div>
				<div class="col-xs-6 text-left">
					<div>

						<img class="section-3 ibeacon lazy wow fadeIn" src="img/OIC_iBeacon.png" alt="">
						<?php
						// include our broadcast, php include due to SVG/CSS3 animations, this skips out the use of something
						// like snap.svg - which is not really needed for this project.
						include( 'img/broadcast.svg' );
						?>

					</div>
					<div class="section-3-tagline">

						<p>
							SMALL iBEACONS ARE PLACED AROUND THE LOCATIONS TO NOTIFY THE VISITORS WHEN THEY GET CLOSE TO
							A HIDDEN STORY.
						</p>

						<img class="wow fadeInRight" src="img/ibeacon-logo_Frit.png" alt="iBeacon Technology">

					</div>
				</div>
			</div>
		</div>
	</div>
</section>