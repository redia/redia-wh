<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 27/02/15
 * Time: 19:33
 */

?>


<section id="section-4">
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-1 col-xs-4 text-center">
				<img class="section-4-ibeacon wow slideInDown" src="img/ibeacon-small.png" alt="">

				<div class="section-4-ibeacon-animation wow fadeInLeft">
					<?php include( 'img/test.svg' ); ?>
				</div>

				<p class="wow fadeInUp" >When you approach an iBeacon, it sends a message to your app via Bluetooth.</p>

			</div>
			<div class="col-xs-2 text-center">
				<img class="section-4-chevron wow fadeIn" src="img/ibeacon-step.png" alt="">
			</div>
			<div class="col-xs-4 text-center">
				<img class="section-4-ipadright wow fadeInRight" src="img/ibeacon-Howto2.png" alt="">
				<p class="wow fadeInUp">Tap OK and an historical figure will appear on your screen that tells you a story from the past.</p>

			</div>
		</div>
	</div>
</section>