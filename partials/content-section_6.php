<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 24/02/15
 * Time: 17:17
 */
?>


<section id="section-6">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center content">

				<!-- Parallax Start -->
				<ul id="scene-section6" class="section-6-scene scene" data-scalar-x="20" data-limit-y="1"   data-invert-y="true">
					<li class="layer " data-depth="0.10" data-limit-y="true">
						<div class="section-6-background"></div>
					</li>
					<li class="layer" data-depth="0.20" data-scalar-x="20" data-scalar-y="8">
						<img class="section-6-ipad-right" src="img/IPAD-Mockup-Section-6.png" alt="">
					</li>
					<li class="layer" data-depth="0.30" data-scalar-x="80"   data-invert-x="true">
						<img class="section-6-man " src="img/PIC-Harald-Blaatand-Frit-.png" alt="">
					</li>
				</ul>

				<!-- Section Header -->

				<img class="divider" alt="" src="img/icon/redbar.png"/>

				<h1 class="wow fadeIn"  data-wow-duration="3s">HISTORICAL FIGURES <br/> BROUGHT TO LIFE</h1>
			</div>
		</div>

		<!-- Section Content -->
		<div class="row">
			<div class="col-xs-6 text-center section-6-left">
<!--				<li class="icon-KNAP-Play section-6-ipad-play"></li>-->
				<img class="section-6-ipad-left glow nudge-up" alt="" src="img/CONTENT-Section-6-ipad-left.png">

				<div class="tagline text-center">
					<p>Experience the sites through <br/>the eyes of historical figures</p>
				</div>
			</div>
		</div>
	</div>
</section>