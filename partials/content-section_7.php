<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 07/03/15
 * Time: 13:45
 */

?>

<section id="section-7">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<!-- Section Header -->
				<img class="divider" alt="" src="img/icon/redbar.png"/>

				<h1 class="wow fadeIn" data-wow-duration="3s">720&deg; Panorama View. <br/> Like Being There</h1>
			</div>
		</div>

		<!-- Section Content -->
		<div class="row pad ">
			<div class="col-xs-12 text-center">
				<div class="section-7-panorama">
					<div id="wrapper">
						<!-- careful with video encoding techniques here, mp4 files must have the atom tag moved to the start for streaming  -->
						<video id="video-360" poster="img/IPAD-360.png" width="880" height="495" loop>
							<source id='mp4'
							        src="img/VID-360.mp4"
							        type='video/mp4'>
							<source id='webm'
							        src="img/VID-360.webm"
							        type='video/webm'>
							<source id='ogv'
							        src="img/VID-360.ogg"
							        type='video/ogg'>
							A HTML5 Browser is required to view this feature.
						</video>
					</div>
					<img class="section-7-ipad" src="img/IPAD-Mockup-transparent.png">
				</div>


			</div>
		</div>

		<!-- Section Content -->
		<div class="row pad ">
			<div class="col-xs-12 text-center">
				<p>TAKE A VIRTUAL TOUR</p>

				<p>- EXPLORE THE SITES FROM YOUR HOME</p>
			</div>
		</div>
	</div>


</section>
