<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 25/02/15
 * Time: 14:05
 */
?>


<section id="section-8">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<!-- Section Header -->
				<img class="divider" alt="" src="img/icon/redbar.png"/>

				<h1 class="wow fadeIn"  data-wow-duration="3s">The Concept</h1>
			</div>
		</div>

		<!-- Section Content -->
		<div class="row pad">
			<div class="col-xs-offset-2 col-xs-8 text-center">
				<div class="section8-text">
					<h2 class="wow fadeInDown" data-wow-delay="0.5s">CHALLENGE</h2>

					<p class="wow fadeIn" data-wow-delay="1.2s">
						Denmark holds three UNESCO World Heritage sites. The sites are very different both regarding
						physical
						expression and period of time they represent. The challenge was to create a digital universe for
						these very different sites to unfold, and at the same time give the under a historical
						experience to
						remember.</p>
				</div>
				<div class="section8-text">
					<h2 class="wow fadeInDown" data-wow-delay="0.6s">INSIGHT</h2>

					<p class="wow fadeIn" data-wow-delay="1.3s">
						We found that the sites have one thing in common – the historical figures and the stories they
						tell.
						Therefore the historical figures became the center point and not the physical sites itself. To
						coin
						the three different World Heritage sites visually we found inspiration in the fantasy universe,
						which allows a mixture of different periods of time. This approach also made sense regarding the
						target group: young users and families.</p>
				</div>
				<div class="section8-text">
					<h2 class="wow fadeInDown" data-wow-delay="0.7s">SOLUTION</h2>

					<p class="wow fadeIn" data-wow-delay="1.5s">
						The app can be used both offsite and offsite. Onsite you can stand face to face with the
						historical
						figures through location based augmented reality, and offsite you can experience 720 degree
						augmented panorama views of the three locations and delve into the gallery of the historical
						gurus.
						Furthermore the sits can be explored through interactive maps, videos, text and images.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>