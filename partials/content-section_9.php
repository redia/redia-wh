<?php
/**
 * www.wh.dev
 * User: ryan
 * Date: 04/03/15
 * Time: 17:37
 */

?>

<section id="section-9">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<!-- Section Header -->
				<img class="divider" alt="" src="img/icon/redbar.png"/>

				<h1 class="wow fadeIn"  data-wow-duration="3s">Behind The Scenes</h1>
			</div>
		</div>

		<!-- Section Content -->
		<div class="row pad">

			<div class="col-xs-4 text-center">
				<img class="section-9-behindscene nudge-up" src="img/PIC-behind-1.jpg" alt="">
			</div>

			<div class="col-xs-4 text-center">
				<img class="section-9-behindscene nudge-up" src="img/PIC-behind-2.jpg" alt="">
				<a class="fancybox" href="https://vimeo.com/110418322">
					<div class="section-9-play icon-KNAP-Play"></div>
				</a>
			</div>

			<div class="col-xs-4 text-center">
				<img class="section-9-behindscene nudge-up" src="img/PIC-behind-3.jpg" alt="">
			</div>

		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<a href="https://itunes.apple.com/dk/app/danmarks-verdensarv/id878924100" target="_blank">
					<div class="section-9-hentapp icon-KNAP-HentApp wow fadeIn"></div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-offset-1 col-xs-5 text-left">
				<img class="section-9-sponsor" src="img/LOGO-unesco.png" width="200" alt="UNESCO">
			</div>
			<div class="col-xs-5 text-right">
				<img class="section-9-sponsor" src="img/Logo-KS.png" width="250" alt="Kultur Styrelsen">
			</div>
		</div>
	</div>
</section>

